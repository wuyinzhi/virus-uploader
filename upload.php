<?php
require_once('VirusTotalApiV2.php');

$target_path = "uploads/";
$max_size = 10 * 1024 * 1024;
$filename = basename($_FILES['fileUpload']['name']);
$target_path = $target_path . $filename; 

$pilihanJenisFile = $_POST['JenisFile'];
if ($pilihanJenisFile == "0") {
	$JenisFile = "malware";
}
else {
	$JenisFile = "false alarm";
}

if ($_FILES['fileUpload']['size'] > $max_size) {
	echo "Ukuran file melebihi batas maksimal.";
} else
{
	if(move_uploaded_file($_FILES['fileUpload']['tmp_name'], $target_path)) {
		echo "Terima kasih. File ".  $filename . " telah diupload. Dilaporkan sebagai $JenisFile.";						
		echo "<br /><br />";
		echo "MD5: " . hash_file('md5', $target_path) . "<br />";
		echo "SHA-1: " . hash_file('sha1', $target_path) . "<br />";
		echo "SHA-256: " . hash_file('sha256', $target_path) . "<br />";
		
		// scan VirusTotal		
		
		$api = new VirusTotalAPIV2('4eb66e5f80567922208a0f116683860c156ee072f7f8245b41463abff1b5a874');  		
		$result = $api->getFileReport(hash_file('md5', $target_path));		
		//$api->displayResult($report);		
?>
		<table>
		  <?php		  
		  $i = 0;
		  echo '<th>No.</th>';
		  echo '<th>Antivirus</th>';		  
		  echo '<th>Terdeteksi sebagai</th>';
		  foreach($result->scans as $key => $val)
		  {			  
			  if(!empty($val->detected)) {
				  echo '<tr>';
				  echo '<td>'.intval($i+1).'.</td>';
				  echo '<td>'.$key.'</td>';
				  echo '<td>'.$val->result.'</td>';
				  echo '</tr>';				
				  $i++;
			  }			  				
		  }
		  ?>
		</table>

<?php	
			
	} else{
		echo "Upload error!";
	}
}
?>